package com.rbmq.springbootrabbitmq.consumer;

import com.rbmq.springbootrabbitmq.config.AcknowledgementConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Author 蜡笔小新
 * @Description 备份队列消费者
 * @Version 1.0
 */
@Slf4j
@Component
public class BackupConsumer {

    @RabbitListener(queues = AcknowledgementConfig.BACKUP_QUEUE)
    public void receiveMsg(Message message){
        log.info("备份交换机{}将消息发{}送到了备份队列{}中",AcknowledgementConfig.BACKUP_EXCHANGE,new String(message.getBody()),AcknowledgementConfig.BACKUP_QUEUE);
    }

}
