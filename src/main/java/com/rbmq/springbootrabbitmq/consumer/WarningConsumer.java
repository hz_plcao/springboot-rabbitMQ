package com.rbmq.springbootrabbitmq.consumer;

import com.rbmq.springbootrabbitmq.config.AcknowledgementConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Author 蜡笔小新
 * @Description 警告队列消费者
 * @Version 1.0
 */
@Slf4j
@Component
public class WarningConsumer {

    @RabbitListener(queues = AcknowledgementConfig.WARNING_QUEUE)
    public void receiver(Message message) {
        log.info("Waring：发现不可路由消息：{}",new String(message.getBody()));
    }
}
