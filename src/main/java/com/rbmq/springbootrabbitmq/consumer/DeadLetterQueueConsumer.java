package com.rbmq.springbootrabbitmq.consumer;

import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @Author 蜡笔小新
 * @Description 基于死信队列实现延迟队列的消费者
 * @Version 1.0
 */
@Slf4j
@Component // 将当前类声明为一个Bean对象
public class DeadLetterQueueConsumer {

    /**
     * @RabbitListener:是Spring AMQP提供的一个注解，用于声明一个监听器，监听指定的RabbitMQ队列
     * @param message
     */
    @RabbitListener(queues = "deadLetterQueue")
    public void receiveMsg(Message message) throws Exception {
        String msg = new String(message.getBody());
        log.info("当前时间；{}，收到死信队列的消息：{}",new Date().toString(),msg);
    }


}
