package com.rbmq.springbootrabbitmq.consumer;

import com.rbmq.springbootrabbitmq.config.AcknowledgementConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Author 蜡笔小新
 * @Description 确认机制的消费者
 * @Version 1.0
 */
@Slf4j
@Component
public class AckConsumer {

    @RabbitListener(queues = AcknowledgementConfig.CONFIRM_QUEUE)
    public void receiveMsg(Message message) {
        String msg = new String(message.getBody());
        log.info("确认机制消费者接收到的消息为：{}", msg);
    }
}
