package com.rbmq.springbootrabbitmq.consumer;

import com.rabbitmq.client.Channel;
import com.rbmq.springbootrabbitmq.config.PriorityQueueConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Author 蜡笔小新
 * @Description 优先级队列的消费者
 * @Version 1.0
 */
@Slf4j
@Component
public class PriorityQueueConsumer {

    @RabbitListener(queues = PriorityQueueConfig.PRIORITY_QUEUE)
    public void receiveMsg(Message message) throws InterruptedException {
        log.info("接收到的优先级队列中的消息：{}", new String(message.getBody()));
    }
}
