package com.rbmq.springbootrabbitmq.consumer;

import com.rbmq.springbootrabbitmq.config.DelayedExchangeConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.util.Date;

/**
 * @Author 蜡笔小新
 * @Description 基于延迟交换机实现的延迟队列的消费者
 * @Version 1.0
 */

@Slf4j
@Component
public class DelayedExchangeConsumer {

//    @RabbitListener(queues = "delayedQueue")
    @RabbitListener(queues = DelayedExchangeConfig.DELAYED_QUEUE)
    public void receiveMsg(Message message) {
        String msg = new String(message.getBody());
        log.info("当前时间：{}，收到延迟队列的消息：{}",new Date().toString(),msg);
    }

}
