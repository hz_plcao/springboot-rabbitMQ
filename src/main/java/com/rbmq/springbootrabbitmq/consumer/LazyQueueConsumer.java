package com.rbmq.springbootrabbitmq.consumer;

import com.rbmq.springbootrabbitmq.config.LazyQueueConfirm;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Author 蜡笔小新
 * @Description 惰性队列消费者
 * @Version 1.0
 */
@Slf4j
@Component
public class LazyQueueConsumer {

    @RabbitListener(queues = LazyQueueConfirm.LAZY_QUEUE_NAME)
    public void receiveMessage(Message message) {
        log.info("惰性队列消息接收成功: {}", new String(message.getBody()));
    }

}
