package com.rbmq.springbootrabbitmq.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @Author 蜡笔小新
 * @Description 消息发送方控制器
 * @Version 1.0
 */
@Slf4j
@RestController
@RequestMapping("/ttl")
public class SendMsgController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 发送消息，不能动态修改消息的TTL
     * @param message
     * @return
     */
    @GetMapping("/sendMsg/{message}")
    public String sendMsg(@PathVariable String message) {
        log.info("当前时间：{}，发送一条消息给两个TTL队列：{}", new Date(), message);
        // convertAndSend是RabbitTemplate提供的一个方法，用于将消息发送到指定的RabbitMQ交换机和路由键中。
        // 可以将消息对象自动转换成指定的消息格式，并发送到指定的目的地
        // 重载：convertAndSend(Object message)：将消息发送到默认交换机中，路由键为空
        // convertAndSend(String exchange, String routingKey, Object message)
        // convertAndSend(String exchange, String routingKey, Object message, MessagePostProcessor messagePostProcessor):
        // 将指定的消息对象发送到指定的交换机和路由键中，并使用指定的 MessagePostProcessor 对消息进行后处理。
        // 如：MessagePostProcessor postProcessor = message -> {
        //    message.getMessageProperties().setExpiration("5000"); // 设置消息过期时间为5秒
        //    return message;
        // };
        // rabbitTemplate.convertAndSend("exchangeB", "routingKeyB", "Hello, RabbitMQ!", postProcessor);
        rabbitTemplate.convertAndSend("normalExchange", "bindingA", "消息来自TTL为10s的队列" + message);
        rabbitTemplate.convertAndSend("normalExchange", "bindingB", "消息来自TTL为40s的队列" + message);
        return "SEND SUCCESS!";
    }

    /**
     * 使用死信队列实现延迟队列并发送一个带有TTL（存活时间）的消息
     *
     * @param message
     * @param ttlTime
     * @return
     */
    @GetMapping("/sendExpirationMsg/{message}/{ttlTime}")
    public String sendMsg(@PathVariable String message, @PathVariable String ttlTime) {

        log.info("当前时间：{}，发送一条时长 {} 毫秒TTL消息给队列QC：{}", new Date().toString(), ttlTime, message);
        MessagePostProcessor postProcessor = msg -> {
            // 设置消息过期时间
            msg.getMessageProperties().setExpiration(ttlTime);
            return msg;
        };
        rabbitTemplate.convertAndSend("normalExchange", "bindingC", message, postProcessor);
        return "MSG SEND SUCCESS!";
    }

    /**
     * 发送消息，使用延迟交换机实现延迟队列
     *
     * @param message
     * @return
     */
    @GetMapping("/sendDelayedMsg/{message}/{delayedTime}")
    public String sendMsg(@PathVariable String message, @PathVariable Integer delayedTime) {
        log.info("当前时间：{}，发送一条时长 {} 毫秒的消息：{}", new Date().toString(), delayedTime, message);
        rabbitTemplate.convertAndSend("delayedExchange", "bindingKey", message, msg -> {
            // getMessageProperties()方法获取消息的属性，并设置消息的延迟时间
            msg.getMessageProperties().setDelay(delayedTime);
            return msg;
        });
        return message+" SEND SUCCESS!";
    }

}
