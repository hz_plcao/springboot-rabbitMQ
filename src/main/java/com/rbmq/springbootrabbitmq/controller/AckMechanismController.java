package com.rbmq.springbootrabbitmq.controller;

import com.rbmq.springbootrabbitmq.config.AcknowledgementConfig;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author 蜡笔小新
 * @Description 确认机制的控制器
 * @Version 1.0
 */
@RestController
@RequestMapping("/ack")
public class AckMechanismController {

    @Autowired
    RabbitTemplate rabbitTemplate;

    @GetMapping("/ackSendMsg/{message}")
    public String sendMsg(@PathVariable String message) {
        // 测试用例1
        // 这里不传参会自动传入UUID
        CorrelationData correlationData1 = new CorrelationData("1");
        rabbitTemplate.convertAndSend(AcknowledgementConfig.CONFIRM_EXCHANGE, AcknowledgementConfig.BINDING_KEY + " ", message + "1", correlationData1);

        // 测试用例2
        CorrelationData correlationData2 = new CorrelationData("2");
        rabbitTemplate.convertAndSend(AcknowledgementConfig.CONFIRM_EXCHANGE, AcknowledgementConfig.BINDING_KEY, message + "2", correlationData2);

        return message + "发送成功！";
    }

}
