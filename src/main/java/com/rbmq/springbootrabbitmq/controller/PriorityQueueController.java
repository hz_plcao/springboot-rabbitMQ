package com.rbmq.springbootrabbitmq.controller;

import com.rbmq.springbootrabbitmq.config.LazyQueueConfirm;
import com.rbmq.springbootrabbitmq.config.PriorityQueueConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author 蜡笔小新
 * @Description 优先级队列Demo的控制器
 * @Version 1.0
 */
@Slf4j
@RestController
@RequestMapping("/msg")
public class PriorityQueueController {

    @Autowired
    private RabbitTemplate rabbitTemplate;




    /**
     * 测试优先级队列
     */
    @GetMapping("/priority/queue")
    public void sendMsgToPriorityQueue() {

        for (int i = 1; i < 2000; i++) {
            if (i == 6) {
                rabbitTemplate.convertAndSend(PriorityQueueConfig.exchangeName,
                        PriorityQueueConfig.routingKey,
                        "priority_message"+i, msg -> {
                            msg.getMessageProperties().setPriority(5);
                            return msg;
                        });
            } else {
                rabbitTemplate.convertAndSend(PriorityQueueConfig.exchangeName,
                        PriorityQueueConfig.routingKey, "message" + i);
            }
        }
    }



}
