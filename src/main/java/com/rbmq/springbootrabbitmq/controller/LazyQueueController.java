package com.rbmq.springbootrabbitmq.controller;

import com.rbmq.springbootrabbitmq.config.LazyQueueConfirm;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author 蜡笔小新
 * @Description TODO
 * @Version 1.0
 */
@RestController
@RequestMapping("/test")
public class LazyQueueController {

    @Autowired
    RabbitTemplate rabbitTemplate;


    /**
     * 惰性队列x
     */
    @GetMapping("/lazy/queue/demo/{message}")
    public void sendMsgToLazyQueue(@PathVariable String message){
        //rabbitTemplate.convertAndSend(LazyQueueConfirm.EXCHANGE_NAME, LazyQueueConfirm.ROUTING_KEY, "测试惰性队列");
        rabbitTemplate.convertAndSend(LazyQueueConfirm.LAZY_QUEUE_NAME, message);
    }
}
