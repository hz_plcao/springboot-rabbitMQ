package com.rbmq.springbootrabbitmq.config;

import com.rabbitmq.client.AMQP;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author 蜡笔小新
 * @Description TTL(存活时间)队列配置类
 * @Version 1.0
 */
@Configuration
public class TTLQueueConfig {

    /**
     * 正常交换机
     */
    public static final String NORMAL_EXCHANGE = "normalExchange";
    /**
     * 死信交换机
     */
    public static final String DEAD_LETTER_EXCHANGE = "deadLetterExchange";
    /**
     * 普通队列
     */
    public static final String QUEUE_A = "QueueA";
    public static final String QUEUE_B = "QueueB";

    public static final String QUEUE_C = "QueueC";
    /**
     * 死信队列
     */
    public static final String DEAD_LETTER_QUEUE = "deadLetterQueue";


    /**
     * 正常交换机
     *
     * @return 返回一个交换机
     */
    @Bean
    public DirectExchange normalExchange() {
        return new DirectExchange(NORMAL_EXCHANGE);
    }

    /**
     * 死信交换机
     *
     * @return 返回一个交换机
     */
    @Bean
    public DirectExchange deadLetterExchange() {
        return new DirectExchange(DEAD_LETTER_EXCHANGE);
    }

    /**
     * 设置队列A
     *
     * @return 返回一个队列
     */
    @Bean
    public Queue queueA() {
        Map<String, Object> arguments = new HashMap<>(3);
        arguments.put("x-dead-letter-exchange", DEAD_LETTER_EXCHANGE);
        arguments.put("x-dead-letter-routing-key", "bindingD");
        arguments.put("x-message-ttl", 10000);
        return QueueBuilder.durable(QUEUE_A).withArguments(arguments).build();
    }

    /**
     * 队列A与交换机绑定
     *
     * @param queueA
     * @param normalExchange
     * @return
     */
    @Bean
    public Binding bindingA(@Qualifier("queueA") Queue queueA, @Qualifier("normalExchange") DirectExchange normalExchange) {
        return BindingBuilder.bind(queueA).to(normalExchange).with("bindingA");
    }

    /**
     * 设置队列B
     *
     * @return 返回一个队列
     */
    @Bean
    public Queue queueB() {
        Map<String, Object> arguments = new HashMap<>(3);
        arguments.put("x-dead-letter-exchange", DEAD_LETTER_EXCHANGE);
        arguments.put("x-dead-letter-routing-key", "bindingD");
        arguments.put("x-message-ttl", 40000);
        return QueueBuilder.durable(QUEUE_B).withArguments(arguments).build();
    }

    /**
     * 队列B与交换机绑定
     *
     * @param queueB
     * @param normalExchange
     * @return
     */
    @Bean
    public Binding bindingB(@Qualifier("queueB") Queue queueB, @Qualifier("normalExchange") DirectExchange normalExchange) {
        return BindingBuilder.bind(queueB).to(normalExchange).with("bindingB");
    }


    /**
     * 死信队列
     *
     * @return 返回一个队列
     */
    @Bean
    public Queue deadLetterQueue() {
        return new Queue(DEAD_LETTER_QUEUE);
    }

    /**
     * 死信队列与死信交换机绑定
     *
     * @param deadLetterQueue
     * @param deadLetterExchange
     * @return
     */
    @Bean
    public Binding deadLetterBinding(@Qualifier("deadLetterQueue") Queue deadLetterQueue, @Qualifier("deadLetterExchange") DirectExchange deadLetterExchange) {
        return BindingBuilder.bind(deadLetterQueue).to(deadLetterExchange).with("bindingD");
    }


    // ##############优化

    /**
     * 通用的延迟队列
     *
     * @return 返回一个队列
     */
    @Bean
    public Queue queueC() {
        Map<String, Object> arguments = new HashMap<>();
        arguments.put("x-dead-letter-exchange", DEAD_LETTER_EXCHANGE);
        arguments.put("x-dead-letter-routing-key", "bindingD");
        return QueueBuilder.durable(QUEUE_C).withArguments(arguments).build();
    }

    /**
     * 与正常交换机进型绑定
     * @param queueC
     * @param normalExchange
     * @return
     */
    @Bean
    public Binding bindingC(Queue queueC, DirectExchange normalExchange) {
        return BindingBuilder.bind(queueC).to(normalExchange).with("bindingC");
    }

}
