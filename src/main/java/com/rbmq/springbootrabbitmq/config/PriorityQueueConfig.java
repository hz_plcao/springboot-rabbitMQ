package com.rbmq.springbootrabbitmq.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author 蜡笔小新
 * @Description 优先级队列
 * @Version 1.0
 */
@Configuration
public class PriorityQueueConfig {


    /**
     * 优先级队列
     */
    public static final String PRIORITY_QUEUE = "priorityQueue";

    /**
     * 交换机
     */
    public static final String exchangeName = "priority.exchange";


    /**
     * 绑定键
     */
    public static final String routingKey = "priority.routingKey";


    /**
     * 构建优先级队列
     *
     * @return
     */
    @Bean
    public Queue priorityQueue() {

        return QueueBuilder.durable(PRIORITY_QUEUE).withArgument("x-max-priority", 10).build();
    }

    /**
     * 构建交换机
     *
     * @return
     */
    @Bean
    public DirectExchange directExchange() {
        return ExchangeBuilder.directExchange(exchangeName).build();
    }

    /**
     * 绑定
     * @param priorityQueue
     * @param directExchange
     * @return
     */
    @Bean
    public Binding queueBindingExchange(Queue priorityQueue, DirectExchange directExchange) {
        return BindingBuilder.bind(priorityQueue).to(directExchange).with(routingKey);
    }


}
