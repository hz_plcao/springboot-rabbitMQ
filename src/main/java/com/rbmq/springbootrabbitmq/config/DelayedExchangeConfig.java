package com.rbmq.springbootrabbitmq.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author 蜡笔小新
 * @Description 延迟交换机配置类
 * @Version 1.0
 */
@Configuration
public class DelayedExchangeConfig {

    /**
     * 延迟交换机
     */
    public static final String DELAYED_EXCHANGE = "delayedExchange";

    /**
     * 延迟队列
     */
    public static final String DELAYED_QUEUE = "delayedQueue";

    /**
     * 绑定Key
     */
    public static final String BINDING_KEY = "bindingKey";

    /**
     * 声明延迟交换机(自定义类型的)
     *
     * @return
     */
    @Bean
    public CustomExchange exchange() {
        Map<String, Object> arguments = new HashMap<>();
        // 表示这个Exchange类型是direct类型，并且支持延迟消息
        arguments.put("x-delayed-type", "direct");
        // “x-delayed-message”不是 RabbitMQ 默认提供的交换机类型，而是由 RabbitMQ Delayed Message Plugin 提供的一种交换机类型，用于实现延迟队列。
        // 在这里使用自定义交换机类型的方式来创建一个延迟队列。
        return new CustomExchange(DELAYED_EXCHANGE, "x-delayed-message", true, false, arguments);
    }

    /**
     * 队列
     *
     * @return
     */
    @Bean
    public Queue queue() {
        return new Queue(DELAYED_QUEUE);
    }

    /**
     * 队列与自定义交换机绑定
     *
     * @return
     */
    @Bean
    public Binding queueBindingDelayedExchange(@Qualifier("queue") Queue queue, @Qualifier("exchange") Exchange exchange) {
        // 这里使用了 noargs() 方法，表示不需要传递任何参数或者额外的信息。
        return BindingBuilder.bind(queue).to(exchange).with(BINDING_KEY).noargs();
    }

}
