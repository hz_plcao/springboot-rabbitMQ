package com.rbmq.springbootrabbitmq.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author 蜡笔小新
 * @Description 确认机制RabbitMQ的配置类
 * @Version 1.0
 */
@Configuration
public class AcknowledgementConfig {

    /**
     * 延迟交换机
     */
    public static final String CONFIRM_EXCHANGE = "confirmExchange";


    /**
     * 备份交换机
     */
    public static final String BACKUP_EXCHANGE = "backupExchange";

    /**
     * 确认队列队列
     */
    public static final String CONFIRM_QUEUE = "confirmQueue";

    /**
     * 备份队列
     */
    public static final String BACKUP_QUEUE = "backupQueue";

    /**
     * 警告队列
     */
    public static final String WARNING_QUEUE = "warningQueue";

    /**
     * 绑定键
     */
    public static final String BINDING_KEY = "key1";


    /**
     * 构建确认交换机
     *
     * @return ：
     */
    @Bean
    public DirectExchange confirmExchange() {
        return ExchangeBuilder
                .directExchange(CONFIRM_EXCHANGE)
                // 设置备份交换机
                .alternate(BACKUP_EXCHANGE)
                .build();
    }

    /**
     * 构建备份交换机
     *
     * @return ：
     */
    @Bean
    public FanoutExchange backupExchange() {
        return ExchangeBuilder.fanoutExchange(BACKUP_EXCHANGE).build();
    }

    /**
     * 构建确认队列
     *
     * @return ：
     */
    @Bean
    public Queue confirmQueue() {
        return QueueBuilder.durable(CONFIRM_QUEUE).build();
    }

    /**
     * 构建备份队列
     *
     * @return ：
     */
    @Bean
    public Queue backupQueue() {
        return QueueBuilder.durable(BACKUP_QUEUE).withArgument("","").build();
    }

    /**
     * 构建警告队列
     *
     * @return ：
     */
    @Bean
    public Queue warningQueue() {
        return QueueBuilder.durable(WARNING_QUEUE).build();
    }

    /**
     * 进行绑定
     *
     * @param confirmQueue
     * @param confirmExchange
     * @return ：
     */
    @Bean
    public Binding confirmQueueBindingConfirmExchange(@Qualifier("confirmQueue") Queue confirmQueue, @Qualifier("confirmExchange") DirectExchange confirmExchange) {
        return BindingBuilder.bind(confirmQueue).to(confirmExchange).with(BINDING_KEY);
    }

    /**
     * 备份队列与备份交换机进行绑定
     *
     * @param backupQueue
     * @param backupExchange
     * @return ：
     */
    @Bean
    public Binding backupQueueBindingBackupExchange(@Qualifier("backupQueue") Queue backupQueue, @Qualifier("backupExchange") FanoutExchange backupExchange) {
        // 扇出类型交换机不需要指定绑定键
        return BindingBuilder.bind(backupQueue).to(backupExchange);
    }

    /**
     * 警告队列与备份交换机绑定
     * @param warningQueue
     * @param warningExchange
     * @return ：
     */
    @Bean
    public Binding warningQueueBindingWarningExchange(@Qualifier("warningQueue") Queue warningQueue, @Qualifier("backupExchange") FanoutExchange warningExchange) {
        return BindingBuilder.bind(warningQueue).to(warningExchange);
    }



}
