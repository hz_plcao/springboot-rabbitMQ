package com.rbmq.springbootrabbitmq.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ReturnedMessage;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @Author 蜡笔小新
 * @Description 回调配置类：异步确认消息是否被Broker（消息代理）接收
 * @Version 1.0
 */
@Slf4j
@Component
public class CallbackConfig implements RabbitTemplate.ConfirmCallback,RabbitTemplate.ReturnsCallback {


    @Autowired
    RabbitTemplate rabbitTemplate;

    /**
     * @PostConstruct //该注解确保CallbackConfig对象创建后立即执行该初始化方法,避免实例化CallbackConfig对象时，rabbitTemplate对象可能还没有被完全初始化
     */
    @PostConstruct
    public void init(){
        // 因为ConfirmCallback是个内部接口，所以需要将CallbackConfig（实现类自身）作为回调函数设置给RabbitTemplate对象，从而调用下方重写的confirm方法来处理消息确认
        rabbitTemplate.setConfirmCallback(this);


        rabbitTemplate.setReturnsCallback(this);
    }

    /**
     * 处理消息的确认：当消息成功发送到RabbitMQ中该方法被调用
     *
     * @param correlationData 发送到RabbitMQ的消息相关数据
     * @param ack             表示消息是否已被RabbitMQ确认接收，如果为true，表示消息已被确认接收，如果为false,表示消息未被确认接收
     * @param cause           表示未能确认消息接收的原因，当ack为false时，该参数将包含错误信息
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {

        // 获取消息Id
        String messageId = correlationData != null ? correlationData.getId() : "";

        if (ack) {
            log.info("确认交换机已收到id为{}的消息", messageId);
        } else {
            log.info("确认交换机未收到Id为{}的消息,原因可能是‘{}’",messageId,cause);
        }

    }

    /**
     * 处理消息的返回：当消息无法被路由到任何队列时，该方法会被调用。可以将消息在传递过程中不达目的地时将消息返回给生产者
     * @param returnedMessage
     */
    @Override
    public void returnedMessage(ReturnedMessage returnedMessage) {
        log.info("消息{}，被交确认换机{}退回，退回原因{}，路由键{}，",
                new String(returnedMessage.getMessage().getBody()),
                returnedMessage.getExchange(),
                returnedMessage.getReplyText(),
                returnedMessage.getRoutingKey()
                );
    }
}
