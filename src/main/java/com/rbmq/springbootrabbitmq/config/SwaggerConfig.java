package com.rbmq.springbootrabbitmq.config;

import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author 蜡笔小新
 * @Description SwaggerConfig配置类,
 * Swagger 是一种 API 文档生成工具，用于生成 RESTful API 接口文档。
 * @version 1.0
 */
@Configuration
@EnableSwagger2 // 启用Swagger
public class SwaggerConfig {

    public Docket webApiConfig(){
        return new Docket(DocumentationType.SWAGGER_2)
                // 指定API的分组名称
                .groupName("webApi")
                // 指定文档的基本信息和描述
                .apiInfo(webApiInfo())
                // 指定需要生成文档的API信息
                .select()
                .build();
    }

    private ApiInfo webApiInfo(){
        return new ApiInfoBuilder()
                // 指定文档的标题
                .title("RabbitMQ接口文档")
                // 指定文档的描述
                .description("本文档描述了RabbitMQ微服务接口定义")
                // 指定文档的版本号
                .version("1.0")
                // 指定文档的联系人信息
                .contact(new Contact("zs","www.at_cpl.com","2105251354@qq.com"))
                .build();
    }
}
