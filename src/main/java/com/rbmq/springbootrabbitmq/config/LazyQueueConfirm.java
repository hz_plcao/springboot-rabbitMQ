package com.rbmq.springbootrabbitmq.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

/**
 * @Author 蜡笔小新
 * @Description 惰性队列配置类, 只有与该队列绑定的消费者消费消息时才会创建
 * @Version 1.0
 */
@Component
public class LazyQueueConfirm {


    /**
     * 队列名称
     */
    public static final String LAZY_QUEUE_NAME = "lazyQueue";


    /**
     * 交换机名称
     */
    public static final String EXCHANGE_NAME = "lazy.exchange";


    /**
     * 绑定键
     */
    public static final String ROUTING_KEY = "lazy.key";


    /**
     * 构建惰性队列
     *
     * @return
     */
    @Bean
    public Queue lazyQueue() {
        return QueueBuilder.durable(LAZY_QUEUE_NAME)
                .withArgument("x-queue-mode", "lazy")
                .build();
    }

//    @Bean
    public DirectExchange lazyExchange() {
        return ExchangeBuilder.directExchange(EXCHANGE_NAME).build();
    }

//    @Bean
    public Binding binding(Queue lazyQueue, DirectExchange lazyExchange) {
        return BindingBuilder.bind(lazyQueue).to(lazyExchange).with(ROUTING_KEY);
    }

}
